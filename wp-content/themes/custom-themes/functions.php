<?php


add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
 
function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

add_action( 'init', 'exam_type' );
function exam_type() {
  $labels = array(
    'name'               => _x( 'Exam Types', 'post type general name' ),
    'singular_name'      => _x( 'Exam Type', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'exam' ),
    'add_new_item'       => __( 'Add New Exam' ),
    'edit_item'          => __( 'Edit Exam Type' ),
    'new_item'           => __( 'New Exam Type' ),
    'all_items'          => __( 'All Exam Types' ),
    'view_item'          => __( 'View Exam Type' ),
    'search_items'       => __( 'Search Exam Type' ),
    'not_found'          => __( 'No Exam Type found' ),
    'not_found_in_trash' => __( 'No Exam Type found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Exam Types'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => '',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array('title', 'editor'),
    'has_archive'   => true,
    'capability_type' => 'page',
    'show_in_rest'	=> true,
    'rest_controller_class' => 'WP_REST_Posts_Controller',
  );
  register_post_type( 'exam_type', $args );
}


add_action( 'init', 'app_user' );
function app_user() {
  $labels = array(
    'name'               => _x( 'App Users', 'post type general name' ),
    'singular_name'      => _x( 'App User', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'user' ),
    'add_new_item'       => __( 'Add New User' ),
    'edit_item'          => __( 'Edit App User' ),
    'new_item'           => __( 'New App User' ),
    'all_items'          => __( 'All App Users' ),
    'view_item'          => __( 'View App User' ),
    'search_items'       => __( 'Search App User' ),
    'not_found'          => __( 'No App User found' ),
    'not_found_in_trash' => __( 'No App User found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'App Users'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => '',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array('title'),
    'has_archive'   => true,
    'capability_type' => 'page',
    'show_in_rest'  => true,
    'rest_controller_class' => 'WP_REST_Posts_Controller',
  );
  register_post_type( 'app_user', $args );
}


add_role(
    'app_user',
    __( 'App User' ),
    array(
        'read'         => true,  // true allows this capability
        'edit_posts'   => false,
        'delete_posts' => false, // Use false to explicitly deny
        'publish_posts' => true
    )
);

function add_theme_caps() {
	$customers = get_role('app_user');
	$customers->add_cap('edit_scores');
	$customers->add_cap('publish_scores');
}
add_action( 'admin_init', 'add_theme_caps', 999);


function mayapp_register_routes() {
	$users_controller = new WP_REST_Users_Controller();
	register_rest_route( 'myapp/v2', '/customers', array(
		array(
			'methods' => WP_REST_Server::CREATABLE,
			'callback' => array($users_controller, 'create_item'),
			'permission_callback' => function( $request ) {
				$request->set_param('roles', array('app_user'));
				// if ( ! current_user_can( 'create_users' ) && $request['roles'] !== array('subscriber')) {

    //                 return new WP_Error(
    //                     'rest_cannot_create_user',
    //                     __( 'Sorry, you are only allowed to create new users with the subscriber role.' ),
    //                     array( 'status' => rest_authorization_required_code() )
    //                 );

    //             }

                return true;
			},
			'args' => $users_controller->get_endpoint_args_for_item_schema( WP_REST_Server::CREATABLE ),
		)
	));
}

add_action( 'rest_api_init', 'mayapp_register_routes' );



// REGISTER ROUTE

// Example
// add_action( 'rest_api_init', function () {
//     register_rest_route( 'skubbs_api/v1', '/author/(?P<id>\d+)', array(
//         'methods' => 'GET',
//         'callback' => 'my_awesome_func',
//     ));
// });
// function my_awesome_func( $data ) {
//     $posts = get_posts( array(
//         'author' => $data['id'],
//     ) );

//     if ( empty( $posts ) ) {
//         return null;
//     }

//     return $posts[0]->post_title;
// }

// Register User
add_action( 'rest_api_init', function () {
    $users_controller = new WP_REST_Users_Controller();
    register_rest_route( 'skubbs-api/v1', '/user-register/', 
        array(
        'methods' => POST,
        'callback' => 'callback_register_user',
    ));
});

function callback_register_user(WP_REST_Request $request) {

    // prepare params
    $user_data = array(

        'user_login' => $request['username'],
        'email' => $request['email'],
        'user_pass' => $request['password'],
        'role' => 'app_user'

    );

    // insert user
    $user_id = wp_insert_user( $user_data ) ;

    //On success
    if ( ! is_wp_error( $user_id ) ) {
        
    } 

    return $user_id;
}

function add_user_details($data) {

    $post = array(
        'post_title' => $data['nice_name'], //first_name + last_name
        'post_type' => 'app_user',
        'post_status' => 'publish',
        'post_content' => ''
    );

    $post_id = wp_insert_post( $post , $wp_error );

    update_field( 'field_5a7d04fa62353', $data['uuid'], $post_id );
    update_field( 'field_5a7d054562356', $data['dob'], $post_id );

    // $att = user_attachment($data['app_user_image'], $post_id);
    var_dump($data['app_user_image']);

    // update_field('field_512e085a9372a', $att['attach_id'], $post_id);
}

function user_attachment($f,$pid,$t='',$c='') {
  wp_update_attachment_metadata( $pid, $f );
  if( !empty( $_FILES[$f]['name'] )) { //New upload
    require_once( ABSPATH . 'wp-admin/includes/file.php' );

    $override['action'] = 'editpost';
    $file = wp_handle_upload( $_FILES[$f], $override );

    if ( isset( $file['error'] )) {
      return new WP_Error( 'upload_error', $file['error'] );
    }

    $file_type = wp_check_filetype($_FILES[$f]['name'], array(
      'jpg|jpeg' => 'image/jpeg',
      'gif' => 'image/gif',
      'png' => 'image/png',
    ));
    if ($file_type['type']) {
      $name_parts = pathinfo( $file['file'] );
      $name = $file['filename'];
      $type = $file['type'];
      $title = $t ? $t : $name;
      $content = $c;

      $attachment = array(
        'post_title' => $title,
        'post_type' => 'attachment',
        'post_content' => $content,
        'post_parent' => $pid,
        'post_mime_type' => $type,
        'guid' => $file['url'],
      );

      foreach( get_intermediate_image_sizes() as $s ) {
        $sizes[$s] = array( 'width' => '', 'height' => '', 'crop' => true );
        $sizes[$s]['width'] = get_option( "{$s}_size_w" ); // For default sizes set in options
        $sizes[$s]['height'] = get_option( "{$s}_size_h" ); // For default sizes set in options
        $sizes[$s]['crop'] = get_option( "{$s}_crop" ); // For default sizes set in options
      }

      $sizes = apply_filters( 'intermediate_image_sizes_advanced', $sizes );

      foreach( $sizes as $size => $size_data ) {
        $resized = image_make_intermediate_size( $file['file'], $size_data['width'], $size_data['height'], $size_data['crop'] );
        if ( $resized )
          $metadata['sizes'][$size] = $resized;
      }

      $attach_id = wp_insert_attachment( $attachment, $file['file'] /*, $pid - for post_thumbnails*/);

      if ( !is_wp_error( $id )) {
        $attach_meta = wp_generate_attachment_metadata( $attach_id, $file['file'] );
        wp_update_attachment_metadata( $attach_id, $attach_meta );
      }

      return array(
            'pid' =>$pid,
            'url' =>$file['url'],
            'file'=>$file,
            'attach_id'=>$attach_id
        );
      // update_post_meta( $pid, 'a_image', $file['url'] );
    }
  }
}

?>