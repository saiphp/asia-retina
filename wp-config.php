<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'asia-retina');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('JWT_AUTH_SECRET_KEY', 'H>]GimtY_:B,%,5b*K80 p:7var~ZV3$=D(W8.>2VV }#2m*JNr tG7#{Am5|apI');

// define('JWT_AUTH_CORS_ENABLE', true);
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5r1ZZsPR+!aFCe3pW^Dw$`n*IvZnl.txdT$a~esmS?|;dD S`LfJS=5Y:%GP$_l$');
define('SECURE_AUTH_KEY',  'p20(^=m_CC/xGIw?K6,_@:<:M@5v& a@?JG8R<YqUc/m+H^ctVqI6f{o8!j7D.1@');
define('LOGGED_IN_KEY',    'O@}[=zzFQYmHsXfj]EF|WrXm=qwOm?G|%kTQk^e|LCNcE($/v#qgo:#}~@WO{c`g');
define('NONCE_KEY',        'e eM029y&TPMNIF0/n NhpL]zrvmzTZt+q{+8Pt1[@|X&u&+7jwT!;/:<>30dlL)');
define('AUTH_SALT',        'ZV<T-_(o2_Q)y*lZ~.R&r$u7S@UBTMJ9BO~Qe#Ct#?e{N( XXp?u-2SO*FgT[FZN');
define('SECURE_AUTH_SALT', '=3JHJDHZc#`N%u<]dpW5}hoei%UJnU^a98!p+:2 XBV(<0=!y]g_GZlAZ-K!vxBm');
define('LOGGED_IN_SALT',   'A2;Yt=)`hc.@<WC|uRZBVV;| `U][*Y]M8ZvEn;-L<DtebtQ~P:63>W^Me[cKr..');
define('NONCE_SALT',       'WGD0hZXzkgU=*Ev<pBk)J>S_?D/%=U}5s9!DcV(6N73r);]orFr0HUz+}<3&/s-/');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
